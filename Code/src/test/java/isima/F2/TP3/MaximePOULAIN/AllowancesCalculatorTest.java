package isima.F2.TP3.MaximePOULAIN;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AllowancesCalculatorTest {

    private final static double delta = 0.0001;

    @Test
    void allowancesDue() {
        assert (Math.abs(AllowancesCalculator.allowancesDue(-10) - 0) < delta);
        assert (Math.abs(AllowancesCalculator.allowancesDue(0) - 0) < delta);
        assert (Math.abs(AllowancesCalculator.allowancesDue(0.1) - 0.15) < delta);
        assert (Math.abs(AllowancesCalculator.allowancesDue(17.123) - 17.85) < delta);
        assert (Math.abs(AllowancesCalculator.allowancesDue(39.5) - 26.80) < delta);
        assert (Math.abs(AllowancesCalculator.allowancesDue(61) - 38) < delta);
        assert (Math.abs(AllowancesCalculator.allowancesDue(81) - 44.81) < delta);
        assert (Math.abs(AllowancesCalculator.allowancesDue(99) - 44.81) < delta);
    }
}