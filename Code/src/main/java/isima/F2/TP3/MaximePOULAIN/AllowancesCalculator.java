package isima.F2.TP3.MaximePOULAIN;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class AllowancesCalculator {

    /**
     * Calcule les indemnités pour les trajets en fonction des kilometres
     *
     * @param kilometers Nombre de kilomètres
     * @return Indemnités
     */
    public static double allowancesDue(double kilometers) {
        double due;
        if (kilometers <= 0) {
            due = 0;
            Main.logger.debug("Class 0 : " + kilometers + " -> " + due);
        } else if (kilometers < 10) {
            due = kilometers * 1.5;
            Main.logger.debug("Class 1 : " + kilometers + " -> " + due);
        } else if (kilometers < 40) {
            due = 10 * 1.5 + (kilometers - 10) * 0.4;
            Main.logger.debug("Class 2 : " + kilometers + " -> " + due);
        } else if (kilometers <= 60) {
            due = 10 * 1.5 + 30 * 0.4 + (kilometers - 40) * 0.55;
            Main.logger.debug("Class 3 : " + kilometers + " -> " + due);
        } else {
            due = 10 * 1.5 + 30 * 0.4 + 20 * 0.55 + 6.81 * (int) ((kilometers - 60) / 20);
            Main.logger.debug("Class 4 : " + kilometers + " -> " + due);
        }
        return round(due, 2);
    }


    private static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }


}
