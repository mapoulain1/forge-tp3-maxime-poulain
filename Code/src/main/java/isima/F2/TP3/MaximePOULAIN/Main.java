package isima.F2.TP3.MaximePOULAIN;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.NoSuchElementException;
import java.util.Scanner;

public class Main {

    public static Logger logger = LoggerFactory.getLogger("isima.F2.TP3.MaximePOULAIN.AllowancesCalculator");

    public static void main(String[] args) {
        logger.warn("Hello world");
        logger.debug(StringUtils.reverse("Maxime POULAIN"));


        if (args.length == 0) {
            handleUserInput();
        } else {
            handleUserCommandLine(args);
        }
    }

    private static void handleUserInput() {
        double kilometers;
        double allowance;
        String input;
        Scanner scanner = new Scanner(System.in);

        logger.info("Entering application with user input.");
        do {
            logger.debug("┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈");
            try {
                input = scanner.nextLine();
                logger.debug("Input : \"" + input + "\"");
            } catch (NoSuchElementException e) {
                logger.info("Exiting application by ctrl + D.");
                return;
            }

            try {
                kilometers = Double.parseDouble(input);
                allowance = AllowancesCalculator.allowancesDue(kilometers);
                logger.info("Calculation : " + kilometers + "km : " + allowance + "€");
            } catch (NumberFormatException e) {
                logger.error("Input error : \"" + input + "\"");
            }

        } while (!input.equalsIgnoreCase("exit"));

        logger.info("Exiting application by exit.");
        scanner.close();
    }

    private static void handleUserCommandLine(String[] args) {
        double kilometers;
        double allowance;

        logger.info("Entering application with command line.");
        for (var arg : args){
            logger.debug("┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈");
            try {
                kilometers = Double.parseDouble(arg);
                allowance = AllowancesCalculator.allowancesDue(kilometers);
                logger.info("Calculation : " + kilometers + "km : " + allowance + "€");
            } catch (NumberFormatException e) {
                logger.error("Input error : \"" + arg + "\"");
            }
        }
        logger.info("Exiting application.");

    }


}

